﻿using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace cSharpRobot
{
    internal static class Tools
    {
        internal static ByteArrayContent GetContent(object x)
        {
            var myContent = JsonConvert.SerializeObject(x);
            var buffer = Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
            return byteContent;
        }

        public static string Post(string call, NameValueCollection parameters)
        {
            var client = new WebClient();

            var x = Properties.Settings.Default.PortalUrl + call;

            var response_data = client.UploadValues(x, "POST", parameters);

            // Parse the returned data (if any) if needed.
            var responseString = UnicodeEncoding.UTF8.GetString(response_data);

            return responseString;
        }
    }
}
