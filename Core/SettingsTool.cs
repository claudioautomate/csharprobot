﻿using Newtonsoft.Json;

namespace cSharpRobot
{
    class SettingsTool
    {
        internal T Get<T>(string s) where T : new()
        {
            return string.IsNullOrEmpty(s) ?
              new T() : JsonConvert.DeserializeObject<T>(s);
        }
    }
}
