﻿using Automate.Portal.Domain.Message;
using Automate.Portal.Domain.Model;
using cSharpRobot.Core;
using MlkPwgen;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace cSharpRobot.Robos.Portal24
{
    class Robo : IRobo
    {
        Conteudo content = new Conteudo();
        ExecucaoIniciada e;
        System.Random ww = new System.Random();
        string tarefa = "Robô em C#";
        string workflowId;
        readonly string uploads = "C:/Program Files/AutoMate Brasil/PGPA/Uploads/";
        BackgroundWorker tb = new BackgroundWorker() { WorkerReportsProgress = true, WorkerSupportsCancellation = true };

        public Robo(BackgroundWorker tb, string workflowId)
        {
            this.workflowId = workflowId;
            this.tb = tb;
        }

        void title(string s)
        {
            tb.ReportProgress(c, $"<h4>{c} - {s}</h4>");
        }

        string GetVar(string s) => e.Parametros.First(t => t.Nome == s).Valor;
        int c = 0;

        public void Run()
        {
            e = IniciarExecucao(workflowId);

            e.Parametros.First(t => t.Nome == "Cliente").Valor = content.Cliente;

            { // Insere registro na fila
                var p = new NameValueCollection
                {
                    { "ExecucaoId", e.ExecucaoId.ToString() },
                    { "Fila", "Consultar" },
                    { "Referencia", content.Cliente},
                    { "Conteudo", JsonConvert.SerializeObject(content) },
                    { "WorkflowId", workflowId.ToString() },
                    { "Funcao", "Inserir Fila" },
                    { "LinhaComando", "1" },
                };
                var result = Tools.Post("/fila/inserir", p);
                c++;
                title("Inserir na fila");
                log(result);
            }

            { // Obtem credencial
                var p = new NameValueCollection
                    {
                        { "ExecucaoId", e.ExecucaoId.ToString() },
                        { "SistemaId", GetVar("SistemaRecFed") },
                        { "Funcao", "Obter Credencial" },
                        { "LinhaComando", "1" },
                    };

                var result = Tools.Post("/execucao/usuario/obter", p);
                c++;
                title("Obter credencial");
                log(result);

                var user = JsonConvert.DeserializeObject<ExecucaoCredencial>(result);
                if (user.Autorizado)
                {
                    p = new NameValueCollection
                    {
                        { "UsuarioId", user.Id.ToString() },
                        { "SenhaAntiga", user.Senha },
                        { "SenhaNova", PasswordGenerator.Generate(8) },
                        { "ExecucaoId", e.ExecucaoId.ToString() },
                        { "Funcao", "Registrar log com anexo" },
                        { "LinhaComando", "1" },
                    };

                    result = Tools.Post("/execucao/usuario/gerar_senha", p);
                    c++;
                    title("Gerar senha");
                    log(result);

                    // Registra log de execucao com anexo
                    p = new NameValueCollection
                        {
                            { "ExecucaoId", e.ExecucaoId.ToString() },
                            { "Tarefa", tarefa },
                            { "Funcao", "Registrar log com anexo" },
                            { "LinhaComando", "1" },
                            { "StatusId", "2" },
                            { "IdentificacaoDocumento", content.Cliente  },
                            { "Descricao", "Acesso realizado com sucesso" },
                        };
                    result = Tools.Post("/execucao/log", p);
                    c++;
                    title("Registrar log");
                    log(result);

                    if (DateTime.Now.Millisecond % 2 == 0)
                    {
                        p = new NameValueCollection
                        {
                            { "UsuarioId", user.Id.ToString() },
                            { "ExecucaoId", e.ExecucaoId.ToString() },
                            { "Funcao", "Desativar usuário" },
                            { "LinhaComando", "1" },
                        };

                        result = Tools.Post("/execucao/usuario/desativar", p);
                        c++;
                        title("Desativar credencial");
                        log(result);
                    }
                }
            }

            { // Fila próximo
                while (true)
                {
                    var p = new NameValueCollection
                    {
                        { "ExecucaoId", e.ExecucaoId.ToString() },
                        { "Fila", "Consultar" },
                        { "Funcao", "Fila próximo" },
                        { "LinhaComando", "1" },
                    };
                    var r = Tools.Post("/fila/proximo", p);
                    c++;
                    title("Próximo da fila");
                    log(r);
                    var dFila = JsonConvert.DeserializeObject<FilaResult<FilaRobo>>(r);
                    if (!dFila.Autorizado) break;

                    var x = $"Iniciando consulta do CNPJ Id: {dFila.Item.Id}, referência: {dFila.Item.Referencia} arquivo de entrada (na maquina do portal): {uploads}{dFila.Item.ArquivoEntrada}{dFila.Item.Conteudo}";
                    tb.ReportProgress(++c, x);

                    {

                        var k = ww.Next(0, 2);

                        p = new NameValueCollection
                        {
                            { "FilaId", dFila.Item.Id.ToString() },
                            { "StatusId", k == 0 ? 3.ToString() : 2.ToString() },
                            { "Mensagem", k == 0 ? "Erro ao processar registro" : "Registro processado com sucesso" },
                            { "ExecucaoId", e.ExecucaoId.ToString() },
                            { "Funcao", "Próximo" },
                            { "LinhaComando", "1" },
                        };

                        r = Tools.Post("/fila/atualizar", p);
                        c++;
                        title("Atualizar fila");
                        log(r);
                    }

                    {
                        p = new NameValueCollection
                        {
                            { "Fila", "Consultar" },
                            { "Referencia", content.Cliente },
                            { "WorkflowId", e.WorkflowId.ToString() },
                            { "ExecucaoId", e.ExecucaoId.ToString() },
                            { "Funcao", "Próximo" },
                            { "LinhaComando", "1" },
                        };
                        r = Tools.Post("/fila/buscar", p);
                        c++;
                        title("Consultar fila");
                        log(r);
                    }


                } // End while
            }

            FinalizarExecucao(e);
        }

        private void log(string result)
        {
            tb.ReportProgress(c, "<pre><code>" + JValue.Parse(result).ToString(Formatting.Indented) + "</code></pre>");
        }

        void FinalizarExecucao(ExecucaoIniciada e, bool Sucesso = true)
        {
            var p = new NameValueCollection
            {
                { "ExecucaoId", e.ExecucaoId.ToString() },
                { "Erro", Sucesso ? "1" : "0" },
                { "Mensagem", null },
                { "Funcao", "FinalizarExecucao" },
                { "LinhaComando", "1" }
            };
            var result = Tools.Post("/execucao/finalizar", p);
            c++;
            title("Finalizar execução");
            log(result);
        }

        ExecucaoIniciada IniciarExecucao(string workflowId)
        {
            ExecucaoIniciada e;
            var p = new NameValueCollection
            {
                { "WorkflowId", workflowId },
                { "Agente", Environment.MachineName },
                { "Tarefa", tarefa },
                { "Funcao", "IniciarExecucao" },
                { "LinhaComando", "1" },
            };
            var result = Tools.Post("/execucao/iniciar", p);
            c++;
            title("Iniciar execução");
            log(result);
            e = JsonConvert.DeserializeObject<ExecucaoIniciada>(result);
            return e;
        }
    }
}