﻿using cSharpRobot.Core;
using cSharpRobot.Robos.Portal24;
using System;
using System.IO;
using System.Windows.Forms;
using TheArtOfDev.HtmlRenderer.WinForms;

namespace cSharpRobot
{
    internal partial class Form1 : Form
    {
        HtmlPanel html => RoboClicked == 1 ? html_1 : html_2;
        HtmlPanel html_1 = new HtmlPanel();
        HtmlPanel html_2 = new HtmlPanel();

        internal Form1()
        {
            InitializeComponent();
            Icon = Properties.Resources.robot;
            bg.ProgressChanged += Bg_ProgressChanged;
            bg.RunWorkerCompleted += Bg_RunWorkerCompleted;
            bg.DoWork += Bg_DoWork;
            html_1.Dock = DockStyle.Fill;
            html_2.Dock = DockStyle.Fill;
            panel1.Controls.Add(html_1);
            panel3.Controls.Add(html_2);
        }

        private void Bg_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            button1.Enabled = true;

            if (erro != null)
            {
                html.Text = erro;
                return;
            }
        }

        string erro;

        private void Bg_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            IRobo robo = null;
            string workflowId = numericUpDownWorkflowId.Value.ToString();

            try
            {
                if (RoboClicked == 1) robo = new Robo(bg, workflowId);
                if (RoboClicked == 2) robo = new RoboCompatibility(bg, workflowId);
                robo.Run();
            }
            catch (System.Exception err)
            {
                erro = $"<b>{err.Message}</b>" + (err.InnerException != null ? "<br><br>" + err.InnerException.Message : "");
            }
        }

        private void Bg_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            html.Text += e.UserState.ToString();
        }
        
        int RoboClicked = 0;

        private void Iniciar()
        {
            if (string.IsNullOrEmpty(textBox2.Text))
            {
                MessageBox.Show("Preencha o endereço do portal");
                return;
            }

            html.ResetText();

            button1.Enabled = false;
            bg.RunWorkerAsync();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            path += "\\Log " + DateTime.Now.ToString("yy-MM-dd hh-mm-ss") + ".html";

            File.WriteAllText(path, html.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            RoboClicked = tabControl1.SelectedIndex + 1;

            Iniciar();
        }
    }
}