﻿using System;

namespace cSharpRobot
{
    class Conteudo
    {
        public DateTime DataAbertura { get; set; }
        public string Cliente { get; set; }
        public string CNPJ { get; set; }
        public bool Ativo { get; set; }

        public Conteudo()
        {
            DataAbertura = DateTime.Now;
            Cliente = "Claudio";
            CNPJ = "78.425.986/0036-15";
            Ativo = true;
        }
    }
}
